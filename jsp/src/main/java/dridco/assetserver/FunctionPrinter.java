package dridco.assetserver;

public interface FunctionPrinter {

	String printFunction(String chooseFunctionName);
}
