package dridco.assetserver;

import static java.lang.String.format;
import static org.apache.commons.lang3.Validate.notEmpty;

public class ResourceTag extends NestedAssetServerTag {

	private static final long serialVersionUID = 5068931448159885591L;

	private String name;

	@Override
	protected void doWithParentTag(AbstractAssetServerTag parent, String tag) {
		parent.addResource(name, tag);
	}

	@Override
	protected String noParentTagMessage() {
		return format("Tag for resource with name %s should be wrapped around a <js /> or <css /> tag", name);
	}

	public void setName(String name) {
		notEmpty(name);
		this.name = name;
	}
}
