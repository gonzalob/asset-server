package dridco.assetserver;

import static dridco.assetserver.AbstractAssetServerTag.ASSET_SERVER_TAG_JS_VAR;
import static java.lang.String.format;
import static org.apache.commons.lang3.Validate.notEmpty;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.jsp.JspException;

public class HereTag extends NestedAssetServerTag {

	private static final long serialVersionUID = 6945279747503174485L;

	private String name;

	@Override
	protected void doWithParentTag(AbstractAssetServerTag parent, String tag) throws JspException {
		try {
			pageContext.getOut().print(renderHereTag(tag, parent));
		} catch (IOException e) {
			throw new JspException(e);
		}
	}

	private String renderHereTag(String tag, AbstractAssetServerTag parent) throws JspException {
		return "<script type='text/javascript'>if('" + tag + "' == " + ASSET_SERVER_TAG_JS_VAR + ") {" + parent.new DocumentWriteCaseBody().write(Arrays.asList(name)) + "}</script>";
	}

	@Override
	protected String noParentTagMessage() {
		return format("Tag for in-place inclusion of resource %s must be defined within a <js /> or <css /> tag", name);
	}

	public void setResource(String name) {
		notEmpty(name);
		this.name = name;
	}
}
