package dridco.assetserver;

import static com.google.common.collect.Lists.newLinkedList;
import static com.google.common.collect.Multimaps.newListMultimap;
import static com.google.common.collect.Multimaps.newSetMultimap;
import static com.google.common.collect.Sets.newLinkedHashSet;
import static java.lang.Boolean.TRUE;
import static java.lang.String.format;
import static javax.servlet.jsp.PageContext.REQUEST_SCOPE;
import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.join;
import static org.apache.commons.lang3.Validate.notEmpty;
import static org.apache.http.HttpHeaders.LOCATION;
import static org.apache.http.client.params.HttpClientParams.setRedirecting;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang3.Validate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

import com.google.common.base.Supplier;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;

public abstract class AbstractAssetServerTag extends TagSupport {

	private static final Log LOG = LogFactory.getLog(AbstractAssetServerTag.class);
	private static final long serialVersionUID = -4794016953911905283L;

	public static final String ASSET_SERVER_BASE_URI_KEY = "asset-server.base-uri";
	public static final String ASSET_SERVER_BASE_URI_ENV = "ASSET_SERVER_BASE_URI";
	public static final String ASSET_SERVER_PASSTHROUGH_URI_KEY = "asset-server.passthrough-uri";
	public static final String ASSET_SERVER_PASSTHROUGH_URI_ENV = "ASSET_SERVER_PASSTHROUGH_URI";
	public static final String AVOID_CACHE_COOKIE_NAME = "ASSET-SERVER_AVOID-CACHE";
	public static final String TAG_DETECTION_FUNCTION_ATTRIBUTE = "_asset_server__detection_function_";

	static final String ASSET_SERVER_TAG_KEY = "_asset_server__tag_";
	static final String ASSET_SERVER_TAG_JS_VAR = "__asset_server_tag";

	private static final String CONTAINED_RESOURCES_ATTRIBUTE_NAME = "_asset_server__contained_resources_";
	private static final String PLAIN_RESOURCES_ATTRIBUTE_NAME = "_asset_server__plain_resources_";
	private static final String TAG_DETECTION_SCRIPT_DONE = "_asset_server__detection_done_";
	private static final String VERSION_PARAMETER = "v";
	private static final String TIMESTAMP_PARAMETER = "d";
	private static final String DEFAULT_TAG_SELECTION_FUNCTION_NAME = "__asset_server_select_tag";

	private static final String CONTENT_HASH_HEADER = "ETag";
	private static final String RESOURCE_PARAMETER = "resource";

	private HttpClient client;
	private HttpParams parameters;
	private String base;
	private String passThrough;

	protected abstract String resourceTypePath();
	protected abstract String includeSourcePattern();

	public AbstractAssetServerTag(HttpClient client) {
		this.client = client;

		parameters = new BasicHttpParams();
		setRedirecting(parameters, false);
	}

	@Override
	public final int doStartTag() {
		try {
			if (noParentTag()) {
				pageContext.setAttribute(ASSET_SERVER_TAG_KEY, this, REQUEST_SCOPE);
				pageContext.setAttribute(CONTAINED_RESOURCES_ATTRIBUTE_NAME, newSetMultimap(Maps.<String, Collection<String>> newHashMap(), new Supplier<Set<String>>() {
					@Override public Set<String> get() { return newLinkedHashSet(); } }));
				pageContext.setAttribute(PLAIN_RESOURCES_ATTRIBUTE_NAME, newListMultimap(Maps.<String, Collection<String>> newHashMap(), new Supplier<List<String>>() {
					@Override public List<String> get() { return newLinkedList(); } }));

				printTagDetectionOnce();
			} else {
				Validate.isAssignableFrom(getClass(), findTagInRequest().getClass(), "Cannot nest different types of tags");
			}
			return EVAL_BODY_INCLUDE;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private void printTagDetectionOnce() throws IOException {
		if (tagDetectionScriptNotYetPrinted()) {
			printTagDetectionFunction();
			printTagDetectionCall();
			tagDetectionScriptDone();
		}
	}

	private void printTagDetectionFunction() throws IOException {
		Object candidate = pageContext.findAttribute(TAG_DETECTION_FUNCTION_ATTRIBUTE);
		Validate.isInstanceOf(FunctionPrinter.class, candidate, format("Could not find an instance of %s under the name %s in any scope", FunctionPrinter.class, TAG_DETECTION_FUNCTION_ATTRIBUTE));
		pageContext.getOut().write(((FunctionPrinter) candidate).printFunction(chooseFunctionName()));
	}

	private void tagDetectionScriptDone() {
		pageContext.setAttribute(TAG_DETECTION_SCRIPT_DONE, TRUE);
	}

	private void printTagDetectionCall() throws IOException {
		String chooseFunctionName = chooseFunctionName();
		pageContext.getOut().print(
			"<!-- http://bit.ly/asset-server --><script type='text/javascript'>" +
			"if(typeof(" + chooseFunctionName + ") != typeof(Function)) throw 'Required function " + chooseFunctionName + "() not found';" +
			"var " + ASSET_SERVER_TAG_JS_VAR + " = " + chooseFunctionName + "();" +
			"</script>");
	}

	private boolean tagDetectionScriptNotYetPrinted() {
		return pageContext.getAttribute(TAG_DETECTION_SCRIPT_DONE) == null;
	}

	private boolean noParentTag() {
		return findTagInRequest() == null;
	}

	private Object findTagInRequest() {
		return requestAttribute(ASSET_SERVER_TAG_KEY);
	}

	@Override
	public final int doEndTag() throws JspException {
		if (handlingNestedTags()) {
			try                   { if (hasResources()) { doPrintTag(pageContext.getOut()); }}
			catch (IOException e) { throw new JspException(e); }
			finally               { pageContext.removeAttribute(ASSET_SERVER_TAG_KEY, REQUEST_SCOPE); }
		}
		return SKIP_BODY;
	}

	private void doPrintTag(JspWriter out) throws JspException, IOException {
		out.print(buildTag());
	}

	private boolean hasResources() {
		return !resources().isEmpty();
	}

	private final String buildTag() throws JspException {
		return selectResources() + selectPlain();
	}

	private String selectResources() throws JspException {
		return renderSelect(resources(), new DocumentWriteCaseBody());
	}

	private String selectPlain() throws JspException {
		return renderSelect(plainResources(), new PlainCaseBody());
	}

	private String renderSelect(Multimap<String, String> resources,
			CaseBody caseBody) throws JspException {
		return hasCases(resources) ? doRenderSelect(resources, caseBody)
				: EMPTY;
	}

	private boolean hasCases(Multimap<String, String> plainResources) {
		return !plainResources.isEmpty();
	}

	private String doRenderSelect(Multimap<String, String> resources, CaseBody caseBody) throws JspException {
		StringBuilder tag = new StringBuilder();
		tag.append(
				"<script type='text/javascript'>" +
				"switch(" + ASSET_SERVER_TAG_JS_VAR + ") {");
		tag.append(renderCases(resources, caseBody));
		tag.append(
				"default: throw 'Detection function returned unknown tag ' + " + ASSET_SERVER_TAG_JS_VAR + ";}" +
				"</script>");
		return tag.toString();
	}

	private StringBuilder renderCases(Multimap<String, String> allResourcesByTag, CaseBody caseBody) throws JspException {
		StringBuilder cases = new StringBuilder();
		for (Map.Entry<String, Collection<String>> resourcesInThisTag : allResourcesByTag.asMap().entrySet()) {
			String tagName = resourcesInThisTag.getKey();
			Collection<String> resources = resourcesInThisTag.getValue();
			if (LOG.isDebugEnabled()) { LOG.debug(format("Rendering case for %s including resources the following resources: %s", tagName, resources)); }
			cases.append(
					"case '" + tagName + "':" +
					caseBody.write(resources) +
					"break;");
		}
		return cases;
	}

	private interface CaseBody {

		String write(Collection<String> resources) throws JspException;
	}

	class DocumentWriteCaseBody implements CaseBody {

		@Override public String write(Collection<String> resources) throws JspException {
			return "document.write('" + format(includeSourcePattern(), escapeHtml4(buildUrl(resources))) + "');";
		}
	}

	private static class PlainCaseBody implements CaseBody {

		@Override public String write(Collection<String> resources) {
			return join(resources.toArray());
		}
	}

	private String chooseFunctionName() {
		return DEFAULT_TAG_SELECTION_FUNCTION_NAME;
	}

	private boolean handlingNestedTags() {
		return findTagInRequest() == this;
	}

	String buildUrl(Collection<String> resources) throws JspException {
		final String uri = uri(resources);
		return resourcesLocation(uri);
	}

	private String uri(Collection<String> resources) {
		return "/" + resourceTypePath() + resourcesAsQueryString(resources);
	}

	private String passThroughBase() throws JspException {
		String passThrough = base(this.passThrough, ASSET_SERVER_PASSTHROUGH_URI_KEY, ASSET_SERVER_PASSTHROUGH_URI_ENV);
		return passThrough != null ? passThrough : assetServerBase();
	}

	private String base(String tagValue, String key, String envVar) throws JspException {
		String            base = findInTag(tagValue);
		if (base == null) base = findInRequest(key);
		if (base == null) base = findInServletContext(key);
		if (base == null) base = findInSystemProperties(key);
		if (base == null) base = findInEnvironmentSettings(envVar);
		return base;
	}

	private String avoidCachingIfApplies() {
		return avoidCaching() ? avoidCachingParameter() : EMPTY;
	}

	private String avoidCachingParameter() {
		return additionalParameter(TIMESTAMP_PARAMETER) + new Date().getTime();
	}

	private boolean avoidCaching() {
		Cookie[] cookies = ((HttpServletRequest) pageContext.getRequest()).getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (AVOID_CACHE_COOKIE_NAME.equals(cookie.getName())) {
					return true;
				}
			}
		}
		return false;
	}

	private String assetServerBase() throws JspException {
		String assetServerBase = base(base, ASSET_SERVER_BASE_URI_KEY, ASSET_SERVER_BASE_URI_ENV);
		if (assetServerBase == null) throw new JspException("Asset Server is not correctly configured");
		return assetServerBase;
	}

	private String findInRequest(String key) {
		Object candidate = requestAttribute(key);
		return candidate != null ? candidate.toString() : null;

	}

	private Object requestAttribute(String key) {
		return pageContext.getAttribute(key, REQUEST_SCOPE);
	}

	private String findInEnvironmentSettings(String envVar) {
		return System.getenv(envVar);
	}

	private String findInSystemProperties(String key) {
		return System.getProperty(key);
	}

	private String findInServletContext(String key) {
		return pageContext.getServletContext().getInitParameter(key);
	}

	private String findInTag(String key) {
		return key;
	}

	private String resourcesLocation(String url) throws JspException {
		try {
			HttpResponse response = assetServerMetadata(url);
			switch (response.getStatusLine().getStatusCode()) {
			case 200:
				return pre18Url(url, response);
			case 302:
				return response.getFirstHeader(LOCATION).getValue() + avoidCachingIfApplies();
			case 404:
				throw resourceNotFound();
			default:
				throw unexpected(response);
			}
		} catch (IOException e) {
			throw connectionFailed(e);
		} catch (URISyntaxException e) {
			throw invalidAssetServerBaseUri(e);
		}
	}

	/**
	 * @deprecated builds a legacy (i.e. a PRE-1.8) URL, for dealing with old servers.
	 */
	@Deprecated
	private String pre18Url(String url, HttpResponse response) throws JspException {
		return assetServerBase() + url + eTagFrom(response) + avoidCachingIfApplies();
	}

	private HttpResponse assetServerMetadata(String url) throws JspException, IOException, URISyntaxException {
		HttpHead request = new HttpHead(passThroughBase() + url);
		request.setHeader("X-Forwarded-Host", new URI(assetServerBase()).getHost());
		request.setParams(parameters);
		return client.execute(request);
	}

	private JspException invalidAssetServerBaseUri(URISyntaxException e) throws JspException {
		throw new JspException(format("Could not parse Asset Server Base URI %s", assetServerBase()), e);
	}

	private JspException connectionFailed(IOException e) throws JspException {
		throw new JspException(format("Connection to Asset Server at %s failed", passThroughBase()), e);
	}

	private JspException unexpected(HttpResponse response) throws JspException {
		throw new JspException(format("Unexpected response from Asset Server at %s: %s", passThroughBase(), response.getStatusLine()));
	}

	private JspException resourceNotFound() throws JspException {
		throw new JspException(format("The Asset Server at %s is missing at least one of the requested %s resources: %s", passThroughBase(), resourceTypePath(), resources()));
	}

	private String eTagFrom(HttpResponse response) {
		String eTag = response.getFirstHeader(CONTENT_HASH_HEADER).getValue();
		return additionalParameter(VERSION_PARAMETER) + stripQuotes(eTag);
	}

	private String stripQuotes(String source) {
		return source.substring(1, source.length() - 1);
	}

	private String resourcesAsQueryString(Collection<String> resources) {
		notEmpty(resources, "This tag requires at least one inner <resource /> tag");
		return firstParameter() + join(resources, additionalParameter(RESOURCE_PARAMETER));
	}

	private String firstParameter() {
		return "?" + parameter(RESOURCE_PARAMETER);
	}

	private String additionalParameter(String parameter) {
		return "&" + parameter(parameter);
	}

	private String parameter(String parameter) {
		return parameter + "=";
	}

	public final void addResource(String resource, String tag) {
		addTo(resource, tag, resources());
	}

	public final void addPlain(String plain, String tag) {
		addTo(plain, tag, plainResources());
	}

	private void addTo(String content, String tag, Multimap<String, String> where) {
		notEmpty(content, "Cannot add an empty resource");
		notEmpty(tag, "Must provide a tag when adding a resource");

		where.put(tag, content);
	}

	public final void setBase(String base) {
		this.base = base;
	}

	public final void setPassThroughBase(String base) {
		passThrough = base;
	}

	private Multimap<String, String> resources() {
		return resourcesFromPageContext(CONTAINED_RESOURCES_ATTRIBUTE_NAME);
	}

	private Multimap<String, String> plainResources() {
		return resourcesFromPageContext(PLAIN_RESOURCES_ATTRIBUTE_NAME);
	}

	@SuppressWarnings("unchecked"	/*
									 * attributes retrieved in this way are handled
									 * by us, so it should be safe to cast them
									 */)
	private Multimap<String, String> resourcesFromPageContext(String name) {
		return (Multimap<String, String>) pageContext.getAttribute(name);
	}
}
