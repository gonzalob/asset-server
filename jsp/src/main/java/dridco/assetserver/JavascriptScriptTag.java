package dridco.assetserver;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

public class JavascriptScriptTag extends AbstractAssetServerTag {

	private static final long serialVersionUID = -4892374441612933339L;

	public JavascriptScriptTag() {
		this(new DefaultHttpClient());
	}

	public JavascriptScriptTag(HttpClient client) {
		super(client);
	}

	@Override
	protected String resourceTypePath() {
		return "js";
	}

	@Override
	protected String includeSourcePattern() {
		return "<scr' + 'ipt type=\"text/javascript\" src=\"%s\"></scr' + 'ipt>";
	}
}
