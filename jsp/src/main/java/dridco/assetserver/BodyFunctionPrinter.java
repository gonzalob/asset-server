package dridco.assetserver;

import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.EMPTY;

public abstract class BodyFunctionPrinter implements FunctionPrinter {

	/**
	 * The script's body. This string is expected to be plain JavaScript code
	 */
	protected abstract String body();

	@Override
	public String printFunction(String chooseFunctionName) {
		return format(
				"%s" +
				"<script type='text/javascript'>" +
				"function %s() { %s }" +
				"</script>" +
				"%s", beforeScript(), chooseFunctionName, body(), afterScript());
	}

	/**
	 * Extension point for rendering some HTML code before the script's body
	 */
	protected String afterScript() {
		return EMPTY;
	}

	/**
	 * Extension point for rendering some HTML code after the script's body
	 */
	protected String beforeScript() {
		return EMPTY;
	}
}
