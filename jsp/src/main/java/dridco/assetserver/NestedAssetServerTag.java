package dridco.assetserver;

import static dridco.assetserver.AbstractAssetServerTag.ASSET_SERVER_TAG_KEY;
import static javax.servlet.jsp.PageContext.REQUEST_SCOPE;
import static org.apache.commons.lang3.Validate.notNull;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

public abstract class NestedAssetServerTag extends BodyTagSupport {

	private static final long serialVersionUID = 6729840007266496557L;

	private String[] tags;

	@Override
	public final int doEndTag() throws JspException {
		final AbstractAssetServerTag parent = (AbstractAssetServerTag) pageContext.getAttribute(ASSET_SERVER_TAG_KEY, REQUEST_SCOPE);
		notNull(parent, noParentTagMessage());
		for (String tag : tags) {
			doWithParentTag(parent, tag);
		}
		return SKIP_BODY;
	}

	protected abstract void doWithParentTag(AbstractAssetServerTag parent, String tag) throws JspException;

	/**
	 * Message to issue when the tag is not used within a parent AbstractAssetServerTag
	 */
	protected abstract String noParentTagMessage();

	public final void setTags(String whitespaceSeparatedTags) {
		Validate.notEmpty(whitespaceSeparatedTags, "The tag name cannot be empty");
		tags = StringUtils.split(whitespaceSeparatedTags);
	}
}
