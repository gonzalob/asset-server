package dridco.assetserver;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

public class StylesheetLinkTag extends AbstractAssetServerTag {

	private static final long serialVersionUID = -960123541756100541L;

	public StylesheetLinkTag() {
		this(new DefaultHttpClient());
	}

	public StylesheetLinkTag(HttpClient client) {
		super(client);
	}

	@Override
	protected String resourceTypePath() {
		return "css";
	}

	@Override
	protected String includeSourcePattern() {
		return "<link rel=\"stylesheet\" type=\"text/css\" href=\"%s\" />";
	}
}
