package dridco.assetserver;

import static java.lang.String.format;

import javax.servlet.jsp.JspException;

public class PlainTag extends NestedAssetServerTag {

	private static final long serialVersionUID = -922742397487334392L;

	@Override
	public int doStartTag() throws JspException {
		return EVAL_BODY_BUFFERED;
	}

	@Override
	protected void doWithParentTag(AbstractAssetServerTag parent, String tag) {
		parent.addPlain(bodyContentAsString(), tag);
	}

	private String bodyContentAsString() {
		return bodyContent.getString();
	}

	@Override
	protected String noParentTagMessage() {
		return format("Tag for plain content can only be declared in the body of a <js /> or <css /> tag. Body content follows:\n%s", bodyContentAsString());
	}
}
