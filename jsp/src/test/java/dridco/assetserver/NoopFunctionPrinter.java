package dridco.assetserver;

import static org.apache.commons.lang3.StringUtils.EMPTY;

public class NoopFunctionPrinter implements FunctionPrinter {

	@Override
	public String printFunction(String chooseFunctionName) {
		return EMPTY;
	}
}
