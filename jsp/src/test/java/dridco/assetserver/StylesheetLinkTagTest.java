package dridco.assetserver;

import org.apache.http.client.HttpClient;

public class StylesheetLinkTagTest extends AbstractAssetServerTagTest {

	@Override
	protected AbstractAssetServerTag newInstance(HttpClient client) {
		return new StylesheetLinkTag(client);
	}

	@Override
	protected String expectedTag(String url) {
		return "<link rel=\"stylesheet\" type=\"text/css\" href=\"" + url + "\" />";
	}
}
