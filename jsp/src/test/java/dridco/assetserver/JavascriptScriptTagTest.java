package dridco.assetserver;

import org.apache.http.client.HttpClient;

public class JavascriptScriptTagTest extends AbstractAssetServerTagTest {

	@Override
	protected AbstractAssetServerTag newInstance(HttpClient client) {
		return new JavascriptScriptTag(client);
	}

	@Override
	protected String expectedTag(String url) {
		return "<scr' + 'ipt type=\"text/javascript\" src=\"" + url + "\"></scr' + 'ipt>";
	}
}
