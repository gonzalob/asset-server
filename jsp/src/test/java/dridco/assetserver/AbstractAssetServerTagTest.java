package dridco.assetserver;

import static dridco.assetserver.AbstractAssetServerTag.TAG_DETECTION_FUNCTION_ATTRIBUTE;
import static dridco.assetserver.HeadWithRequestUrlPrefix.headToUriStartingWith;
import static javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE;
import static org.apache.http.HttpStatus.SC_MOVED_TEMPORARILY;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.servlet.jsp.JspException;

import org.apache.http.ProtocolVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicStatusLine;
import org.hamcrest.Description;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.springframework.mock.web.MockPageContext;

public abstract class AbstractAssetServerTagTest {

	protected static final String BASE = "http://localhost";

	private static final String ANY_TAG = "default";

	private AbstractAssetServerTag tested;
	private MockPageContext pageContext;
	private HttpClient httpClient;

	protected abstract AbstractAssetServerTag newInstance(HttpClient client);

	protected abstract String expectedTag(String url);

	@Before
	public void setUp() {
		pageContext = new MockPageContext();
		pageContext.setAttribute(TAG_DETECTION_FUNCTION_ATTRIBUTE, new NoopFunctionPrinter());
		httpClient = mock(HttpClient.class);

		tested = newInstance(httpClient);
		tested.setPageContext(pageContext);

		tested.setBase(BASE);
	}

	@Test
	public void thatItOnlyPrintsTheTagSelectionWhenHasNoResourcesDefined() throws JspException, UnsupportedEncodingException {
		exerciseWithResources();
		assertEquals("<!-- http://bit.ly/asset-server --><script type='text/javascript'>if(typeof(__asset_server_select_tag) != typeof(Function)) throw 'Required function __asset_server_select_tag() not found';var __asset_server_tag = __asset_server_select_tag();</script>", pageContext.getContentAsString());
	}

	@Test
	public void afterStartingTagTheBodyIsProcessed() throws JspException {
		assertEquals(EVAL_BODY_INCLUDE, tested.doStartTag());
	}

	@Test
	public void buildsTagWithOneResource() throws IOException, JspException {
		String foundAt = foundAt();
		when(httpClient.execute(Matchers.<HttpUriRequest> any())).thenReturn(httpResponse(foundAt));

		exerciseWithResources("foo");

		assertEquals(expectedScript(foundAt), pageContext.getContentAsString());
	}

	private String foundAt() {
		return BASE + "?tag=hash";
	}

	@Test
	public void buildsTagWithTwoResources() throws IOException, JspException {
		String foundAt = foundAt();
		when(httpClient.execute(Matchers.<HttpUriRequest> any())).thenReturn(httpResponse(foundAt));

		exerciseWithResources("foo", "bar");

		assertEquals(expectedScript(foundAt), pageContext.getContentAsString());
	}

	@Test
	public void usesCustomPassthroughBaseWhenDefined() throws IOException, JspException {
		String customPassThrough = "http://cdn.internal";

		when(httpClient.execute(Matchers.<HttpUriRequest> any())).thenReturn(httpResponse(foundAt()));

		tested.setPassThroughBase(customPassThrough);
		exerciseWithResources("anything");

		verify(httpClient).execute(argThat(headToUriStartingWith(customPassThrough)));
	}

	@Test
	public void thatItRendersAnInPlaceTag() throws ClientProtocolException, IOException, JspException {
		when(httpClient.execute(Matchers.<HttpUriRequest> any())).thenReturn(httpResponse(foundAt()));
		tested.doStartTag();

		HereTag inPlace = new HereTag();
		inPlace.setPageContext(pageContext);
		inPlace.setTags(ANY_TAG);
		inPlace.setResource("inplace");
		inPlace.doEndTag();

		assertEquals(
				"<!-- http://bit.ly/asset-server --><script type='text/javascript'>if(typeof(__asset_server_select_tag) != typeof(Function)) throw 'Required function __asset_server_select_tag() not found';var __asset_server_tag = __asset_server_select_tag();</script>" +
				"<script type='text/javascript'>" +
				"if('" + ANY_TAG + "' == __asset_server_tag) {" +
				"document.write('" + expectedTag(foundAt()) + "');" +
				"}</script>",
				pageContext.getContentAsString());
	}

	@Test
	public void thatItRendersPlainResources() throws ClientProtocolException, IOException, JspException {
		String plain = "any plain thing;";
		String foundAt = foundAt();

		when(httpClient.execute(Matchers.<HttpUriRequest> any())).thenReturn(httpResponse(foundAt));

		tested.doStartTag();
		tested.addResource("foo", ANY_TAG);
		tested.addPlain(plain, ANY_TAG);
		tested.doEndTag();

		assertEquals(
				"<!-- http://bit.ly/asset-server --><script type='text/javascript'>if(typeof(__asset_server_select_tag) != typeof(Function)) throw 'Required function __asset_server_select_tag() not found';var __asset_server_tag = __asset_server_select_tag();</script>" +
				"<script type='text/javascript'>" +
				"switch(__asset_server_tag) {" +
				"case '" + ANY_TAG + "':" +
				"document.write('" + expectedTag(foundAt) + "');" +
				"break;" +
				"default: throw 'Detection function returned unknown tag ' + __asset_server_tag;" +
				"}" +
				"</script>" +
				"<script type='text/javascript'>" +
				"switch(__asset_server_tag) {" +
				"case '" + ANY_TAG + "':" +
				plain +
				"break;" +
				"default: throw 'Detection function returned unknown tag ' + __asset_server_tag;" +
				"}" +
				"</script>",
				pageContext.getContentAsString());
	}

	private void exerciseWithResources(String... resources) throws JspException {
		tested.doStartTag();
		for (String resource : resources) {
			tested.addResource(resource, ANY_TAG);
		}
		tested.doEndTag();
	}

	private BasicHttpResponse httpResponse(String url) {
		BasicHttpResponse response = new BasicHttpResponse(new BasicStatusLine(new ProtocolVersion("HTTP", 1, 1), SC_MOVED_TEMPORARILY, "Found"));
		response.addHeader("Location", url);
		return response;
	}

	private String expectedScript(String location) {
		return	"<!-- http://bit.ly/asset-server --><script type='text/javascript'>if(typeof(__asset_server_select_tag) != typeof(Function)) throw 'Required function __asset_server_select_tag() not found';var __asset_server_tag = __asset_server_select_tag();</script>" +
				"<script type='text/javascript'>" +
				"switch(__asset_server_tag) {" +
				"case '" + ANY_TAG + "':" +
				"document.write('" + expectedTag(location) + "');" +
				"break;" +
				"default: throw 'Detection function returned unknown tag ' + __asset_server_tag;" +
				"}" +
				"</script>";
	}
}

class HeadWithRequestUrlPrefix extends org.hamcrest.TypeSafeMatcher<org.apache.http.client.methods.HttpHead> {

	private String prefix;

	public static HeadWithRequestUrlPrefix headToUriStartingWith(String prefix) {
		return new HeadWithRequestUrlPrefix(prefix);
	}

	private HeadWithRequestUrlPrefix(String prefix) {
		this.prefix = prefix;
	}

	@Override
	protected boolean matchesSafely(org.apache.http.client.methods.HttpHead item) {
		return item.getRequestLine().getUri().startsWith(prefix);
	}

	@Override
	public void describeTo(Description description) {
		description.appendText("a HEAD request to an URI starting with ");
		description.appendValue(prefix);
	}
}
