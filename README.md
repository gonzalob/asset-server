# What is it?
Asset Server is an effort to tackle some of the most recurring performance issues 
in web applications. Best practices recommend packing and minifying resources, 
and maximizing cache usage. We're trying to solve this in a clean way.

# Development
The project is hosted at [Bitbucket](https://bitbucket.org/gonzalob/asset-server).
Source code, documentation and bugtracking are all located there.

# Building
Assembly is done using [Gradle Wrapper](http://bit.ly/1erKeaq). Getting a copy 
of the source tree and invoking it should be enough to get all modules built.

# Requirements
Having Java 1.6 or greater installed should be enough to get any module up and 
running. If you're a Linux user, your distro will most probably have a package 
for it. Otherwise, check [Java.com](http://www.java.com/es/download/manual.jsp)
for a suitable package.

# Licensing
Please read the LICENSE file.

