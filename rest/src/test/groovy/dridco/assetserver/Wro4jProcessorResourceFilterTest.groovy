package dridco.assetserver

import org.junit.Test
import ro.isdc.wro.model.resource.processor.ResourcePostProcessor

class Wro4jProcessorResourceFilterTest {

    @Test
    void 'uses the implemented processor to filter resources'() {
        Wro4jProcessorResourceFilter tested = new StubWro4jProcessorResourceFilter()
        tested.filter "anything"
        assert tested.called
    }

    class StubWro4jProcessorResourceFilter extends Wro4jProcessorResourceFilter {

        boolean called = false

        @Override
        ResourcePostProcessor processor() {
            new ResourcePostProcessor() {

                @Override
                void process(Reader reader, Writer writer) throws IOException {
                    called = true
                }
            }
        }
    }
}
