package dridco.assetserver

import static org.hamcrest.Matchers.hasItems
import static org.junit.Assert.assertThat
import static org.junit.Assert.fail

import org.junit.Test

class ResourceTypeTest {

    TrackInvocationFilter tracker = new TrackInvocationFilter()
    ResourceType          tested  = anyResourceType()

	String firstSampleIdentifier  = 'google-analytics'
	String firstSampleResource    = 'alert("hello google");'
	String firstSampleHash        = 'a0d7cd45dbff190aaa30c9fbd298fd20b9462c5e'
	String secondSampleIdentifier = 'piwik-analytics'
	String secondSampleResource   = 'alert("hello piwik");'

    @Test
    void "applies the resource type's filter chain before returning the resources"() {
        tested.store firstSampleIdentifier, firstSampleResource
        tested.load(fromStorage(firstSampleIdentifier))
        assert tracker.invoked
    }

    @Test
    void 'retrieves a resource it knows about'() {
        tested.store firstSampleIdentifier, firstSampleResource
        assert firstSampleResource == tested.load(fromStorage(firstSampleIdentifier))
    }

	@Test
	void 'that it loads resources in the expected order'() {
		tested.store firstSampleIdentifier, firstSampleResource
		tested.store secondSampleIdentifier, secondSampleResource
		assert secondSampleResource + firstSampleResource == tested.load(fromStorage(secondSampleIdentifier, firstSampleIdentifier))
	}

	@Test
	void 'fails to retrieve an unknown resource'() {
		try {
			tested.load(fromStorage(firstSampleIdentifier))
			fail 'Successfully loaded a non existing resource'
		} catch (ResourceNotFound e) {
			assert firstSampleIdentifier == e.id
		}
	}

    @Test
    void 'storing under an existing identifier updates the resource'() {
        tested.store firstSampleIdentifier, secondSampleResource
        tested.store firstSampleIdentifier, firstSampleResource
        assert firstSampleResource == tested.load(fromStorage(firstSampleIdentifier))
    }

    @Test
    void 'can list all known keys for a given type'() {
        tested.store firstSampleIdentifier, firstSampleResource
        assertThat tested.list(), hasItems(firstSampleIdentifier)
    }

    private Text anyResourceType() {
        new Text(filter: tracker)
    }

	def fromStorage(String... identifiers) {
		identifiers.collect { new StoredContent(identifier: it) }
	}
}
