package dridco.assetserver

import org.junit.Test

class AssetServerTest {

           Text javaScriptResources = new Text       (storage: new HashMapStorage())
    AssetServer assetServer         = new AssetServer(types  : ['javascript': javaScriptResources])

	@Test
	void 'loads the resource of known type'() {
		assert javaScriptResources == assetServer.of('javascript')
	}

	@Test(expected = UnsupportedResourceType)
	void 'fails to retrieve an unknown type'() {
		assetServer.of 'unknown'
	}
}
