package dridco.assetserver

class TrackInvocationFilter implements ResourceFilter {

    boolean invoked = false

    @Override
    def filter(Object source) {
        invoked = true
        source
    }
}
