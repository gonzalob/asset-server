package dridco.assetserver.controllers

import static dridco.assetserver.Profiles.TESTING
import static dridco.assetserver.model.Constants.RESOURCE_PARAMETER_NAME
import static dridco.assetserver.model.Constants.VERSION_PARAMETER_NAME
import static org.hamcrest.Matchers.equalTo
import static org.hamcrest.Matchers.is
import static org.springframework.http.HttpMethod.HEAD
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup

import java.awt.PageAttributes.MediaType;

import groovy.transform.NotYetImplemented

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.web.context.WebApplicationContext

import dridco.assetserver.ApplicationConfiguration
import dridco.assetserver.AssetServer
import dridco.assetserver.RestConfiguration

@RunWith(SpringJUnit4ClassRunner)
@WebAppConfiguration
@ContextConfiguration(classes = [ApplicationConfiguration, RestConfiguration])
@ActiveProfiles(TESTING)
abstract class TextResourceControllerTest {

    abstract urlMapping()
    abstract resourceType()
	abstract mediaType()

    @Autowired
    AssetServer server
    @Autowired
    WebApplicationContext context
    MockMvc mvc
	@Autowired
	List<TextResourceController> controllers
	
    @Before
    void setup() {
        mvc = webAppContextSetup(context).build()
		commit()
    }

    @After
    void tearDown() {
		def resourceType = server.of(resourceType())
        new ArrayList(resourceType.list()).each { resourceType.remove it }
		
		controllers.each {
			it.commands.eTagsCache.with {
				resources.clear()
			}
		}
    }

    @Test
    void 'not providing a resource in a request lists all known resources'() {
        def key = 'elmer'
        storeAndCommit key, 'fudd'
        mvc.perform(get(urlMapping())).
                andExpect header().string('Link', "<${resourceUri key}>;title=\"${key}\"")
    }

    @Test
    void 'requesting a non-existing resource fails with not found'() {
        mvc.perform(get(url('unknown'))).
                andExpect status().isNotFound()
    }

    @Test
    void 'putting a non-existing resource returns status ok'() {
        storeAndCommit('barney', 'stinson').andExpect status().isCreated()
    }

    @Test
    void 'putting a non-existing resource points to that resource'() {
        def key = 'ally'
        storeAndCommit(key, 'mcbeal').
                andExpect header().string('location', equalTo(resourceUri(key)))
    }

    @Test
    void 'after putting a resource, it can be retrieved'() {
        def name = 'neal'
        def value = 'caffrey'
        storeAndCommit(name, value)
        mvc.perform(get(url(name))).
                andExpect content().string(value)
    }

    @Test
    void 'can retrieve two resources'() {
        def firstName = 'rick'
        def firstValue = 'grimes'
        def secondName = 'daryl'
        def secondValue = 'dixon'
        storeAndCommit firstName, firstValue
        storeAndCommit secondName, secondValue
        mvc.perform(get(urlMapping()).param(RESOURCE_PARAMETER_NAME, firstName).param(RESOURCE_PARAMETER_NAME, secondName)).
                andExpect status().isFound()
    }

	@Test
	void "that retrieving more than one resource points to the current resources' location"() {
		def firstName = 'jack'
		def firstValue = 'crawford'
		def secondName = 'hannibal'
		def secondValue = 'lecter'
		storeAndCommit firstName, firstValue
		storeAndCommit secondName, secondValue
		mvc.perform(get(urlMapping()).param(RESOURCE_PARAMETER_NAME, firstName).param(RESOURCE_PARAMETER_NAME, secondName)).
				andExpect header().string('Location', "${baseUri()}?${VERSION_PARAMETER_NAME}=5a0274159e6e73a4029208d348020f724a235da5&${VERSION_PARAMETER_NAME}=74313dadd90ba4df3210cf9374a5c0328aa99042") 
	}

	@Test
	void 'that retrieving two resources joins them together'() {
		def firstValue  = 'cooper'
		def secondValue = 'wolowitz'
		store          'sheldon', firstValue
		storeAndCommit 'howard', secondValue
		mvc.perform(get(urlMapping()).param(VERSION_PARAMETER_NAME, '166adf7cb43fc4d37ee98226d117b953bcf79516')
									 .param(VERSION_PARAMETER_NAME, 'b98768a4688a832d4548c8d8988c49297c3faf33')).
				andExpect content().string(firstValue + secondValue)
	}

    @Test
    void 'retrieving an existing resource and one that does not exist returns not found'() {
        def existing = 'fred'
        storeAndCommit existing, 'flintstone'
        mvc.perform(get(urlMapping()).param(RESOURCE_PARAMETER_NAME, existing).param(RESOURCE_PARAMETER_NAME, 'non existing')).
                andExpect status().isNotFound()
    }

    @Test
    void 'getting a resource has its associated e-tag'() {
        def name = 'luke'
        storeAndCommit name, 'skywalker'
        mvc.perform(get(url(name))).
                andExpect header().string('ETag', is('"b002c355e99cc30c9dd4a91b9498df56d151a2f9"'))
    }

    @Test
    void 'requesting a resource with its current e-tag results in not modified response'() {
        def name = 'emily'
        storeAndCommit name, 'grayson'
        mvc.perform(get(url(name)).header('if-none-match', '"a8cc47f90f4883e85c2f097d69393902346431fc"')).
                andExpect status().isNotModified()
    }

    @Test
    void 'heading a resource has its associated e-tag'() {
        def name = 'bart'
        storeAndCommit name, 'simpson'
        mvc.perform(request(HEAD, url(name))).
                andExpect header().string('ETag', is('"df43ad44d44e898f8f4e6ed91e6952bfce573e12"'))
    }

    @Test
    void 'after deleting a resource, it is not found'() {
        def name = 'walter'
        storeAndCommit name, 'white'
		deleteAndCommit name, '528cef87d0bfb947548ab94679d1e5765f19089a'

        mvc.perform(get(url(name))).
                andExpect status().isNotFound()
    }

    @Test
    void 'deleting a resource with an invalid eTag fails with precondition failed'() {
        def name = 'bruce'
        storeAndCommit name, 'wayne'
        mvc.perform(delete(url(name)).header('if-match', '"i am not a valid eTag"')).
                andExpect status().isPreconditionFailed()
    }

    @Test
    void 'when a resource is changed, any request containing it has a new eTag'() {
        def secondName = 'thor'
        store          'tony', 'stark'
        storeAndCommit secondName, ''

        // change one of the resources, and verify the change does happen
        mvc.perform(put(url(secondName)).content('odinson').header('if-match', '"da39a3ee5e6b4b0d3255bfef95601890afd80709"')).
                andExpect status().isOk()
		commit()

        mvc.perform(get(urlMapping()).param(VERSION_PARAMETER_NAME, 'ec595fb76d78bdcef2af244a13e87b44b9c61e9d').param(VERSION_PARAMETER_NAME, '8bee256eedd896312001faa537dfe37d8bb7177f')).
                andExpect header().string('ETag', '"d1c73f4d880e7d2587639a29a47d5f1d1cb80c54"')
    }

	@Test
	void "when a resource is changed, the resource's location changes accordingly"() {
		def firstName  = 'abu'
		def secondName = 'carrie'
		store          firstName, 'nazir'
		storeAndCommit secondName, ''

		updateAndCommit secondName, 'mathison', 'da39a3ee5e6b4b0d3255bfef95601890afd80709'	

		mvc.perform(get(urlMapping()).param(RESOURCE_PARAMETER_NAME, firstName).param(RESOURCE_PARAMETER_NAME, secondName)).
				andExpect header().string('Location', "${baseUri()}?${VERSION_PARAMETER_NAME}=c91246dcb7c2e7e4363e1506dcc8324d05e306ed&${VERSION_PARAMETER_NAME}=920ebc47579eefa4496ef471994ceb4cd16d734e")

	}

	@Test
	void 'that it provides a range if containing too many resources'() {
		storeTooManyResources()
		mvc.perform(get(baseUri())).
			andExpect header().string('Accept-Ranges', "page")
	}

	@Test
	void 'that it provides information on the range on a ranged request'() {
		storeTooManyResources()
		mvc.perform(get(baseUri()).header('Range', 'page=1')).
			andExpect header().string('Content-Range', 'page 1/2')
	}

	@Test
	void 'that it provides the second set of resources when requested'() {
		storeTooManyResources()
		mvc.perform(get(baseUri()).header('Range', 'page=2')).
			andExpect header().string('Content-Range', 'page 2/2')
	}

	@Test
	void 'that it has status of bad request if requesting an invalid page'() {
		storeTooManyResources()
		mvc.perform(get(baseUri()).header('Range', 'page=3')).
			andExpect status().isBadRequest()
	}

	@Test
	void 'that it has status of bad request if requesting more than one page'() {
		storeTooManyResources()
		mvc.perform(get(baseUri()).header('Range', 'page=1-2')).
			andExpect status().isBadRequest()
	}

	@Test
	void 'that it has status of entity too large if not specifying pages when required'() {
		storeTooManyResources()
		mvc.perform(get(baseUri())).
			andExpect status().isRequestEntityTooLarge()
	}

	@Test
	void 'that it outputs the powered-by header'() {
		mvc.perform(get(urlMapping())).
			andExpect header().string('Powered-By', 'Asset Server <http://bit.ly/asset-server>')

	}
	
	@Test
	void 'that it outputs the cache control header'() {
		def name = 'clark'
		def value = 'kent'
		storeAndCommit name, value
		
		mvc.perform(get(url(name))).
			andExpect header().string('Cache-Control', 'public, max-age=31536000')
	}

	@Test
	void 'that it is not in staging mode when started'() {
		mvc.perform(get(urlMapping())).
			andExpect header().string('X-AssetServer-Mode', 'Normal')
	}

	@Test
	void 'that after adding a resource the server enters staging mode'() {
		store 'nicholas', 'brody'
		mvc.perform(get(urlMapping())).
				andExpect header().string('X-AssetServer-Mode', 'Staging')
	}

	@Test
	void 'that after committing the server returns to normal mode'() {
		storeAndCommit 'bruce', 'wayne'
		commit()
		mvc.perform(get(urlMapping())).
				andExpect header().string('X-AssetServer-Mode', 'Normal')
	}

	@Test
	void 'that after deleting a resource the server enters staging mode'() {
		store 'charles', 'xavier'
		mvc.perform(get(urlMapping())).
				andExpect header().string('X-AssetServer-Mode', 'Staging')
	}

	@Test
	void 'that after updating a resource the server enters staging mode'() {
		store 'peter', 'griffin'
		mvc.perform(get(urlMapping())).
				andExpect header().string('X-AssetServer-Mode', 'Staging')
	}

	@Test
	void 'that a new resource is not delivered while on staging'() {
		def name = 'sherlock'

		mvc.perform put(url(name)).content('holmes')
		mvc.perform(get(url(name))).
				andExpect status().isNotFound()
	}

	@Test
	void 'that a modified resource points to the old etag while on staging'() {
		def name = 'john'
		storeAndCommit name, 'snow'
		update         name, 'lennon', 'b94e9f3d7e001981b2dd49f2a70822a8ac8f3e68'
		mvc.perform(get(urlMapping()).param(RESOURCE_PARAMETER_NAME, name)).
				andExpect header().string('Location', "${baseUri()}?${VERSION_PARAMETER_NAME}=b94e9f3d7e001981b2dd49f2a70822a8ac8f3e68")
	}

	@Test
	void 'that a modified resource dumps the old etag header'() {
		def name = 'spencer'
		storeAndCommit name, 'hastings'
		update         name, 'reid', '8e9c4393a43f3efc8f72ba8ad8d05fb98144435b'
		mvc.perform(get(url(name))).
				andExpect header().string('ETag', '"8e9c4393a43f3efc8f72ba8ad8d05fb98144435b"')
	}

	@Test
	void 'that a deleted resource exists while on staging'() {
		def name = 'gregor'
		storeAndCommit name, 'clegane'

		delete name, 'ef84c24f2a7c74271deba9ac435149cc11e21739'

		mvc.perform(get(url(name))).
				andExpect status().isOk()
	}

	@Test
	void 'that it sets the right media type when getting the current version of a resource'() {
		def name = 'ethel'
		storeAndCommit name, 'anderson'

		mvc.perform(get(url(name))).
			andExpect header().string('Content-Type', mediaType())
	}

	@Test
	void 'that it sets the right media type when getting joint sources'() {
		store          'glenn', 'quagmire'
		storeAndCommit 'joe', 'swanson'

		mvc.perform(get(urlMapping()).param(VERSION_PARAMETER_NAME, 'b4279ee40916a76f5e47d276e42226d0bc8ee078').param(VERSION_PARAMETER_NAME, '946d65c1b13e3c543d59c716976af80f2d1c1242')).
			andExpect header().string('Content-Type', mediaType())

	}

	private storeTooManyResources() {
		(1..21).each { store it.toString(), 'any value' }
		commit()
	}

    private String url(name) {
        "${urlMapping()}/${name}"
    }

    def storeAndCommit(String name, String value) {
        def stored = store name, value
		commit()
		stored
    }
	
	def store(String name, String value) {
		mvc.perform put(url(name)).content(value)
	}

	def updateAndCommit(String name, String value, String eTag) {
		def updated = update name, value, eTag
		commit()
		updated
	}

	def update(String name, String value, String eTag) {
		mvc.perform put(url(name)).header('if-match', "\"${eTag}\"").content(value)
	}

    def deleteAndCommit(String name, String eTag) {
        def deleted = delete name, eTag
		commit()
		deleted
    }
	
	def delete(String name, String eTag) {
		mvc.perform delete(url(name)).header('if-match', "\"${eTag}\"")
	}

    def resourceUri(id) {
        "${hostUri()}${url id}".toString()
    }

	def baseUri() {
		"${hostUri()}${urlMapping()}".toString()
	}

	def hostUri() {
		'http://localhost'
	}

	private commit() {
		mvc.perform(post('/commit')).andExpect status().isOk()
	}
}
