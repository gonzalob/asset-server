package dridco.assetserver.controllers

import static dridco.assetserver.Profiles.TESTING
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.web.context.WebApplicationContext

import dridco.assetserver.ApplicationConfiguration
import dridco.assetserver.RestConfiguration
import dridco.assetserver.WebSecurityConfiguration

@RunWith(SpringJUnit4ClassRunner)
@WebAppConfiguration
@ContextConfiguration(classes = [ApplicationConfiguration, WebSecurityConfiguration, RestConfiguration])
@ActiveProfiles(TESTING)
class OperationModeControllerTest {

	@Autowired
	FilterChainProxy springSecurityFilterChain
	@Autowired
	WebApplicationContext context
	MockMvc mvc

	@Before
	void setup() {
		mvc = webAppContextSetup(context).addFilter(springSecurityFilterChain).build()
	}

	@Test
	void 'that committing requires authentication'() {
		mvc.perform(post('http://localhost:8080/commit'))
			.andExpect status().isUnauthorized()
	}
}
