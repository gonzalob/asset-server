package dridco.assetserver.controllers

class StylesheetsControllerTest extends TextResourceControllerTest {

    @Override
    def urlMapping() {
        '/css'
    }

    @Override
    def resourceType() {
        'stylesheets'
    }

	@Override
	def mediaType() {
		'text/css'
	}
}
