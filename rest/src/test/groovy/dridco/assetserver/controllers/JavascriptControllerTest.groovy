package dridco.assetserver.controllers

class JavascriptControllerTest extends TextResourceControllerTest {

    @Override
    def urlMapping() {
        '/js'
    }

    @Override
    def resourceType() {
        'javascript'
    }

	@Override
	def mediaType() {
		'application/javascript'
	}
}
