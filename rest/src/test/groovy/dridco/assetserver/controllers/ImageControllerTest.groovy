package dridco.assetserver.controllers

import dridco.assetserver.ApplicationConfiguration
import dridco.assetserver.RestConfiguration
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.web.context.WebApplicationContext

import static dridco.assetserver.Profiles.TESTING
import static org.hamcrest.Matchers.equalTo
import static org.hamcrest.Matchers.is
import static org.springframework.http.HttpMethod.HEAD
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup

@RunWith(SpringJUnit4ClassRunner)
@WebAppConfiguration
@ContextConfiguration(classes = [ApplicationConfiguration, RestConfiguration])
@ActiveProfiles(TESTING)
class ImageControllerTest {

    static def URL_MAPPING = '/images'

    @Autowired
    WebApplicationContext context
    MockMvc mvc

    @Before
    void setup() {
        mvc = webAppContextSetup(context).build()
    }

    @Test
    void 'requesting a non-existing resource fails with not found'() {
        mvc.perform(get(url('unknown'))).
                andExpect status().isNotFound()
    }

    @Test
    void 'putting a non-existing resource returns status ok'() {
        store('logo', [23, 11, 53, 23] as byte[]).
                andExpect status().isCreated()
    }

    @Test
    void 'putting a non-existing resource points to that resource'() {
        def key = 'spacer'
        store(key, [22, 77, 32, 66] as byte[]).
                andExpect header().string('location', equalTo(resourceUri(key)))
    }

    @Test
    void 'after putting a resource, it can be retrieved'() {
        def name = 'corner-left'
        byte[] value = [33, 66, 88, 21]
        store name, value
        mvc.perform(get(url(name))).
                andExpect content().bytes(value)
    }

    @Test
    void 'getting a resource has its associated e-tag'() {
        def name = 'footer-background'
        store name, [36, 24, 76, 11] as byte[]
        mvc.perform(get(url(name))).
                andExpect header().string('ETag', is('"e47d404a3a603951af2565a10c42fc2489a72674"'))
    }

    @Test
    void 'requesting a resource with its current e-tag results in not modified response'() {
        def name = 'banner'
        store name, [66, 23, 55,  1] as byte[]
        mvc.perform(get(url(name)).header('if-none-match', '"4d1f856145a22ef38f37a8b789e4735c0f72d46c"')).
                andExpect status().isNotModified()
    }

    @Test
    void 'heading a resource has its associated e-tag'() {
        def name = 'small-logo'
        store name, [63, 62, 73, 23] as byte[]
        mvc.perform(request(HEAD, url(name))).
                andExpect header().string('ETag', is('"7b3e8514ece65a99e23fb4e81fa2feb803558fe5"'))
    }

    @Test
    void 'after deleting a resource, it is not found'() {
        def name = 'arrow-up'
        store name, [ 8, 23, 53, 12] as byte[]
        mvc.perform delete(url(name)).header('if-match', '"d32b5b08c1cf602cd2db6b5042eee1fde34c05f2"')
		commit()
        mvc.perform(get(url(name))).
                andExpect status().isNotFound()
    }

    @Test
    void 'deleting a resource with an invalid eTag fails with precondition failed'() {
        def name = 'error-cross'
        store name, [55, 82, 13, 68] as byte[]
        mvc.perform(delete(url(name)).header('if-match', '"i am not a valid eTag"')).
                andExpect status().isPreconditionFailed()
    }

    private static String url(name) {
        "${URL_MAPPING}/${name}"
    }

    private store(String name, byte[] value) {
        def store = mvc.perform put(url(name)).content(value)
		commit()
		store
    }

	private commit() {
		mvc.perform(post('/commit')).andExpect status().isOk()
	}

    private static resourceUri(id) {
        "http://localhost${url id}".toString()
    }
}
