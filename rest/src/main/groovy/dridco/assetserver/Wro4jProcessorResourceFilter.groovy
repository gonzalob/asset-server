package dridco.assetserver

import static org.apache.commons.io.IOUtils.closeQuietly
import groovy.util.logging.Slf4j
import ro.isdc.wro.model.resource.processor.ResourcePostProcessor

@Slf4j
abstract class Wro4jProcessorResourceFilter implements ResourceFilter {

	abstract ResourcePostProcessor processor()

	@Override
	final filter(Object source) {
		StringReader reader
		StringWriter out
		try {
			reader = new StringReader(source)
			out = new StringWriter()
			processor().process reader, out
			out.toString()
		} catch(Exception e) {
			log.warn "Could not filter resource. Returning as-is", e
			source
		} finally {
			closeQuietly(reader)
			closeQuietly(out)
		}
	}
}
