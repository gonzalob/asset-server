package dridco.assetserver

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

import dridco.assetserver.environments.ResourceTypes

@Configuration
@ComponentScan(basePackageClasses = ResourceTypes)
class ApplicationConfiguration {

	@Autowired
	ResourceTypes resourceTypesDefinition

	@Bean
	AssetServer assetServer() {
		Map<String, ResourceType> types = new HashMap<>()
		ResourceTypes.methods.each {
			types.put it.name, it.invoke(resourceTypesDefinition)
		}
		new AssetServer(types: types)
	}
}
