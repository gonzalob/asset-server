package dridco.assetserver

interface Profiles {

	String TESTING     = 'testing'
	String DEVELOPMENT = 'development'
	String PRODUCTION  = 'production'
}
