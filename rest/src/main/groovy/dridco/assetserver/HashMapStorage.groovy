package dridco.assetserver

import static java.util.Collections.unmodifiableSet

class HashMapStorage implements Storage {

	private store = [:].withDefault { Serializable it -> throw new ResourceNotFound(id: it) }

	@Override
	Collection<Serializable> keys() {
		unmodifiableSet(store.keySet())
	}

	@Override
	Serializable load(Serializable key) throws ResourceNotFound {
		store.get key
	}

	@Override
	void store(Serializable key, Serializable value) {
		store.put key, value
	}

	@Override
	void delete(Serializable key) throws ResourceNotFound {
		if (!store.containsKey(key)) throw new ResourceNotFound(id: key)
		store.remove key
	}
}
