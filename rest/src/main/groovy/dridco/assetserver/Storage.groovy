package dridco.assetserver

interface Storage {

	/**
	  * Lists all known keys
	  *
	  * @return an {@java.lang.Iterable} of all stored keys, or an empty one. Never null.
	  */
	Collection<Serializable> keys()

	/**
	  * Retrieves the value for the given key.
	  *
	  * @throws ResourceNotFound if the Storage is not holding a value for the specified key.
	  */
	Serializable load(Serializable key) throws ResourceNotFound

	/**
	  * Stores the given value under the specified key.
	  * If the key already exists, the value is updated.
	  *
	  * This is an optional operation, as the underlying storage may or may not support being written to.
	  *
	  * @throws UnsupportedOperationException if this is not a Storage that can be written to.
	  */
	void store(Serializable key, Serializable value)

	/**
	  * Deletes the value stored under the given key.
	  *
	  * @throws ResourceNotFound if attempting to delete something that does not exist.
	  * @throws UnsupportedOperationException if the Storage does not support deletions.
	  */
	void delete(Serializable key) throws ResourceNotFound
}
