package dridco.assetserver

class StoredContent implements ContentSource {

	def identifier

	@Override
	def getContent(Storage storage) {
		storage.load identifier
	}
}
