package dridco.assetserver

class InexistingContent implements ContentSource {

	@Override
	def getContent(Storage storage) {
		throw new ResourceNotFound()
	}
}
