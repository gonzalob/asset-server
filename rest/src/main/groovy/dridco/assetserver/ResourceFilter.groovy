package dridco.assetserver

interface ResourceFilter {

	/**
	 * Execute some transformation on the source, prior to returning it to the client
	 * The source will be already joined one after another, if more than one resource
	 * was requested.
	 */
	def filter(source)
}
