package dridco.assetserver

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping

import dridco.assetserver.controllers.ControllerConstants

@Configuration
@ComponentScan(basePackageClasses = ControllerConstants)
class RestConfiguration extends WebMvcConfigurationSupport {

	@Override
	RequestMappingHandlerMapping requestMappingHandlerMapping() {
		new RequestMappingHandlerMapping( useSuffixPatternMatch: false )
	}
}
