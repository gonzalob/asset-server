package dridco.assetserver.model;

import static java.util.concurrent.TimeUnit.DAYS;

public interface Constants {

	Long   DEFAULT_CACHE_MAX_AGE    = DAYS.toSeconds(365);

	String SKIP_FILTERS_COOKIE_NAME = "SKIP_ASSET_SERVER_FILTERS";
	String RESOURCE_PARAMETER_NAME  = "resource";
	String VERSION_PARAMETER_NAME   = "tag";
	String IF_MATCH                 = "if-match";
	String IF_NONE_MATCH            = "if-none-match";
}
