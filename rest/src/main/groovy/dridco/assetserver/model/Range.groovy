package dridco.assetserver.model;

import static org.apache.commons.lang3.Validate.isTrue

import org.springframework.http.HttpHeaders

import com.google.common.base.Optional

abstract class Range {

	static final Integer RESOURCES_PER_PAGE = 20
	static final String  RANGE_TYPE         = 'page'

	private static final int    TYPE_HEADER_POSITION  = 0
	private static final int    VALUE_HEADER_POSITION = 1
	private static final String TYPE_VALUE_SEPARATOR  = '='

	protected abstract void process(resource, Closure closure)
	protected abstract boolean handles(HttpHeaders links, Collection resources)
	protected abstract Throwable fail()

	static Range parse(Optional<String> header) {
		if(header.isPresent()) { parseRangeHeader(header.get()) }
		else                   { new NoRange()                  }
	}

	static PageRange parseRangeHeader(String header) {
		try      { return new PageRange(page: findPageIn(header)) }
		catch(e) {  throw new PageRangeParseException()           }
	}

	private static Integer findPageIn(String header) {
		String[] typeAndValue = header.split(TYPE_VALUE_SEPARATOR)
		isTrue(RANGE_TYPE == typeAndValue[TYPE_HEADER_POSITION])
		Integer.valueOf(typeAndValue[VALUE_HEADER_POSITION])
	}

	void handle(links, resources, closure) {
		if(handles(links, resources)) {
			resources.each { process it, closure }
		} else { throw fail() }
	}
}
