package dridco.assetserver.model

import static dridco.assetserver.model.Constants.*
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo
import static org.springframework.http.HttpStatus.*
import groovy.util.logging.Slf4j

import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity

import dridco.assetserver.ExternalContent
import dridco.assetserver.ResourceType
import dridco.assetserver.StoredContent

@Slf4j
class Commands {

	private               cacheMaxAge     = Constants.DEFAULT_CACHE_MAX_AGE
	private               controller
	private ETags         eTagsCache
	private ResourceType  resources
	private Closure       responseHeaders = { it }
	private MediaType     mediaType       = MediaType.ALL
	private               mode            = [toNormal:{ doResumeNormalOperation() }]

	ResponseEntity list(Range page) {
		def resources = resources.list()
		def headers = baseHeaders()
		page.handle(headers, resources) {
			headers.add 'Link', "<${linkTo(controller.class).slash(it).toString()}>;title=\"${it}\""
		}
		new ResponseEntity(headers, NO_CONTENT)
	}

	ResponseEntity current(String resource, String eTags, boolean skipFilters) {
		if (eTagsCache.matched(resource, eTags, skipFilters, mode)) {
			new ResponseEntity(NOT_MODIFIED)
		} else {
			new ResponseEntity(
					resources.load([ mode.contentSourceCollector.call(resource) ], skipFilters),
					httpHeaders() { eTagsCache.eTag resource, skipFilters, mode },
					OK)
		}
	}

	ResponseEntity get(Iterable<String> hashes, String eTags, boolean skipFilters) {
		if (eTagsCache.matched(hashes, eTags, skipFilters, mode)) {
			new ResponseEntity(NOT_MODIFIED)
		} else {
			new ResponseEntity(
					resources.load(hashes.collect(eTagsCache.resourceName).collect(mode.contentSourceCollector), skipFilters),
					httpHeaders() { eTagsCache.eTag hashes, skipFilters, mode },
					OK)
		}
	}

	ResponseEntity redirect(Iterable<String> ids) {
		def target = linkTo(controller.class).toString() + "?${VERSION_PARAMETER_NAME}=" + ids.collect { eTagsCache.eTag(it, false, mode) }.join("&${VERSION_PARAMETER_NAME}=")
		HttpHeaders headers = new HttpHeaders([
			'Location': target
		])
		new ResponseEntity(headers, FOUND)
	}

	ResponseEntity head(String id) {
		head() { eTagsCache.eTag id, false, mode }
	}

	ResponseEntity head(List<String> hashes) {
		head() { eTagsCache.eTag hashes, false, mode }
	}

	ResponseEntity head(Closure eTag) {
		def headers = httpHeaders(eTag)
		new ResponseEntity(headers, OK)
	}

	ResponseEntity put(String id, String eTags, Serializable source) {
		try {
			if (eTagsCache.matched(id, eTags, true, mode)) {
				checkDifferencesAndStore id, source
			} else {
				new ResponseEntity(PRECONDITION_FAILED)
			}
		} catch (ResourceNotFound) {
			/* if the resource does not exist, it is safe to put it regardless of any eTag */
			mode.add id, source
		}
	}

	ResponseEntity delete(String id, String eTags) {
		if (eTagsCache.matched(id, eTags, false, mode)) {
			mode.delete id
		} else {
			def eTag = eTagsCache.eTag(id, false, mode)
			log.debug "Could not delete ${id}. Client knows version(s) ${eTags}, current is ${eTag}"
			new ResponseEntity(PRECONDITION_FAILED)
		}
	}

	void resumeNormalOperation() {
		mode.toNormal()
	}

	void doResumeNormalOperation() {
		log.debug "Resuming normal operation on commands for ${controller}"
		mode = new Normal(commands: this)
	}

	void enterStaging() {
		log.info 'Entering staging mode'
		mode = new Staging(commands: this, resources: resources)
	}

	def enterStagingAndAdd(Serializable id, Serializable source) {
		enterStaging()
		mode.add id, source
	}

	def enterStagingAndUpdate(Serializable id, Serializable source) {
		enterStaging()
		mode.update id, source
	}

	def enterStagingAndRemove(Serializable id) {
		enterStaging()
		mode.delete id
	}

	def add(Serializable id, Serializable source) {
		doStore id, source, CREATED
	}

	def update(Serializable id, Serializable source) {
		doStore id, source
	}

	def remove(Serializable id) {
		resources.remove id
		eTagsCache.evict id
		log.debug "Deleted entity with id ${id}"
		new ResponseEntity(OK)
	}

	private checkDifferencesAndStore(String id, Serializable source) {
		def current = resources.load(Arrays.asList(new StoredContent(identifier: id)), true)
		if (current == source) {
			log.info "Not storing up-to-date resource ${id}"
		} else {
			mode.update id, source
		}
	}

	private doStore(String id, Serializable source, HttpStatus status = OK) {
		log.info "Storing resource with id ${id}"
		resources.store id, source
		eTagsCache.evict id
		new ResponseEntity(new HttpHeaders([
			eTag    : "\"${eTagsCache.eTag(id, false) { new ExternalContent(content: source) }}\"".toString(),
			location: linkTo(controller.class).slash(id).toUri()
		]), status)
	}

	private HttpHeaders baseHeaders() {
		new HttpHeaders([
			'Powered-By'        : 'Asset Server <http://bit.ly/asset-server>',
			'X-AssetServer-Mode': mode.class.simpleName
		])
	}

	private HttpHeaders httpHeaders(Closure eTag) {
		def headers = baseHeaders()
		headers.setAll([
			'Cache-Control': "public, max-age=${cacheMaxAge}".toString(),
			'ETag'         : "\"${eTag.call()}\"".toString(),
			'Content-Type' : mediaType.toString()
		])
		responseHeaders.call headers
		headers
	}
}
