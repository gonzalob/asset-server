package dridco.assetserver.model

import dridco.assetserver.StoredContent

class Normal implements OperationMode {

	Commands commands
	def      contentSourceCollector = { new StoredContent(identifier: it) }
	def      cache                  = [:]

	@Override
	def add(Serializable id, Serializable source) {
		commands.enterStagingAndAdd id, source
	}

	@Override
	def update(Serializable id, Serializable source) {
		commands.enterStagingAndUpdate id, source
	}

	@Override
	def delete(Serializable id) {
		commands.enterStagingAndRemove id
	}

	@Override
	void toNormal() { /* no-op */ }
}
