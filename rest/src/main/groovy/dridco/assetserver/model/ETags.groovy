package dridco.assetserver.model

import groovy.util.logging.Slf4j

import org.apache.commons.codec.digest.DigestUtils

import com.google.common.collect.ArrayListMultimap
import com.google.common.collect.Multimap

import dridco.assetserver.ResourceType

@Slf4j
class ETags {

	private ResourceType type
	private Multimap     resources = ArrayListMultimap.create()

	def sourceLoader = { Iterable<String> list, boolean skip, contentSourceCollector ->
		type.load(list.collect(contentSourceCollector), skip)
	}

	def resourceName = { resources.get(it)[0] ?: it }

	String eTag(String resource, boolean skipFilters, final OperationMode mode) {
		mode.cache.withDefault { eTag it, skipFilters, mode.contentSourceCollector }[resource]
	}

	String eTag(String resource, boolean skipFilters, Closure contentSourceCollector) {
		def name = resourceName.call resource
		def source = sourceLoader.call([name], skipFilters, contentSourceCollector)
		def eTag = DigestUtils.sha1Hex source
		log.info "Computed eTag value for ${name}: ${eTag}"
		resources.put eTag, name
		eTag
	}

	String eTag(Iterable<String> resources, boolean skipFilters, OperationMode mode) {
		log.debug "Computing compound eTag for ${resources}"
		def computed = resources.collect {
			log.debug "Adding $it to compound eTag"
			eTag it, skipFilters, mode.contentSourceCollector
		}.join()
		def eTag = DigestUtils.sha1Hex computed
		log.debug "Compound eTag for ${resources} is ${eTag}"
		eTag
	}

	boolean matched(String resource, String clientSent, boolean skipFilters, OperationMode mode) {
		def current = "\"${eTag resource, skipFilters, mode}\""
		clientSent.split(',').contains current
	}

	boolean matched(Iterable<String> resources, String clientSent, boolean skipFilters, OperationMode mode) {
		def current = "\"${eTag resources, skipFilters, mode}\""
		clientSent.split(',').contains current
	}

	void evict(String eTag) {
		log.info "Evicting ${eTag}"
		def toRemove = resources.entries().findAll { it.value == eTag }.collect { it.key }
		toRemove.each { resources.remove it, eTag }
	}
}
