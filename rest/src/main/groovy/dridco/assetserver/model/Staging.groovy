package dridco.assetserver.model

import dridco.assetserver.ExternalContent
import dridco.assetserver.InexistingContent
import dridco.assetserver.ResourceType
import dridco.assetserver.StoredContent

class Staging implements OperationMode {

	Commands     commands
	ResourceType resources
	def          addedResources   = []
	def          updatedResources = [:]
	def          cache            = [:]

	def contentSourceCollector = {
		if(addedResources.contains(it))           { new InexistingContent() }
		else if(updatedResources.containsKey(it)) { new ExternalContent(content: updatedResources.get(it)) }
		else                                      { new StoredContent(identifier: it) }
	}

	@Override
	def add(Serializable id, Serializable source) {
		addedResources << id
		commands.add id, source
	}

	@Override
	def update(Serializable id, Serializable source) {
		updatedResources.put id, resources.load([ new StoredContent(identifier: id) ])
		commands.update id, source
	}

	@Override
	def delete(Serializable id) {
		updatedResources.put id, resources.load([ new StoredContent(identifier: id) ])
		commands.remove id
	}

	@Override
	void toNormal() {
		commands.doResumeNormalOperation()
	}
}
