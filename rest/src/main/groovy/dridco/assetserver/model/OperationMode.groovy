package dridco.assetserver.model

interface OperationMode {

	def add(Serializable id, Serializable source)
	def update(Serializable id, Serializable source)
	def delete(Serializable id)
	void toNormal()
}
