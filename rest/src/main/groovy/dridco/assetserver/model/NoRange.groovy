package dridco.assetserver.model;

import org.springframework.http.HttpHeaders

public class NoRange extends Range {

	@Override
	public void process(resource, Closure closure) {
		closure.call resource
	}

	@Override
	boolean handles(HttpHeaders links, Collection resources) {
		resources.size() <= RESOURCES_PER_PAGE
	}

	@Override
	Throwable fail() {
		throw new RangeRequired()
	}
}
