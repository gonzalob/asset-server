package dridco.assetserver.model

import static java.lang.Math.ceil
import static java.lang.Math.max

import org.springframework.http.HttpHeaders

class PageRange extends Range {

	Integer page
	private int i = 0

	@Override
	public void process(resource, Closure closure) {
		if(i++ in (pageStart()..pageEnd())) {
			closure.call resource
		}
	}

	def pageStart() { (page - 1) * RESOURCES_PER_PAGE     }
	def pageEnd()   {  page      * RESOURCES_PER_PAGE - 1 }

	@Override
	boolean handles(HttpHeaders links, Collection resources) {
		Integer totalPages = totalPages(resources.size())
		links.set('Content-Range', "page ${page}/${totalPages}")
		page <= totalPages
	}

	Integer totalPages(size) {
		max ceil(size.div(RESOURCES_PER_PAGE)), 1
	}

	@Override
	Throwable fail() {
		throw new PageRangeParseException()
	}
}
