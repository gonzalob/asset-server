package dridco.assetserver

import groovy.transform.ToString
import ro.isdc.wro.model.resource.processor.impl.css.JawrCssMinifierProcessor

@ToString
class JawrCssMinifierProcessorResourceFilter extends Wro4jProcessorResourceFilter {

	@Override
	JawrCssMinifierProcessor processor() {
		new JawrCssMinifierProcessor()
	}
}
