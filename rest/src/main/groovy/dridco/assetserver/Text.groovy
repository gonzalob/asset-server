package dridco.assetserver

class Text extends ResourceType {

	@Override
	def process(resources) {
		resources.collect { new String(it) }.join ''
	}
}
