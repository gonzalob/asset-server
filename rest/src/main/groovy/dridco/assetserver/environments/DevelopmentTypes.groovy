package dridco.assetserver.environments

import static dridco.assetserver.Profiles.DEVELOPMENT

import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile

import dridco.assetserver.JawrCssMinifierProcessorResourceFilter
import dridco.assetserver.JsMinProcessorResourceFilter

@Configuration
@Profile(DEVELOPMENT)
class DevelopmentTypes extends VolatileEnvironmentTypes {

	@Override
	def javascriptFilter() {
		new JsMinProcessorResourceFilter()
	}

	@Override
	def stylesheetFilter() {
		new JawrCssMinifierProcessorResourceFilter()
	}
}
