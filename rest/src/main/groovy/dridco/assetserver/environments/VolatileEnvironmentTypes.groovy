package dridco.assetserver.environments

import groovy.util.logging.Slf4j
import dridco.assetserver.HashMapStorage
import dridco.assetserver.Image
import dridco.assetserver.ResourceType
import dridco.assetserver.Text

@Slf4j
abstract class VolatileEnvironmentTypes implements ResourceTypes {

	@Override
	ResourceType stylesheets() {
		textResourceType('stylesheet', stylesheetFilter())
	}

	@Override
	ResourceType javascript() {
		textResourceType('javascript', javascriptFilter())
	}

	@Override
	ResourceType images() {
		log.warn 'images will be stored in memory'
		new Image(storage: new HashMapStorage())
	}

	abstract javascriptFilter()
	abstract stylesheetFilter()

	static textResourceType(type, filter) {
		log.warn "${type} resources will be stored in volatile memory, applying ${filter}"
		new Text(storage: new HashMapStorage(), filter: filter)
	}
}
