package dridco.assetserver.environments

import static dridco.assetserver.Profiles.PRODUCTION
import groovy.util.logging.Slf4j

import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile

import dridco.assetserver.*

@Configuration
@Profile(PRODUCTION)
@Slf4j
class ProductionTypes implements ResourceTypes {

	@Override
	ResourceType javascript() {
		textResourceType('js', new JsMinProcessorResourceFilter())
	}

	@Override
	ResourceType stylesheets() {
		textResourceType('css', new JawrCssMinifierProcessorResourceFilter())
	}

	@Override
	ResourceType images() {
		buildResourceType('images') { new Image(storage: it) }
	}

	static ResourceType textResourceType(id, filter) {
		buildResourceType(id) { storage ->
			new Text(storage: storage, filter: filter)
		}
	}

	static ResourceType buildResourceType(id, builder) {
		def root = new File(System.properties["${id}.root"] as String ?: "/var/lib/assets/${id}")
		if (root.exists()) {
			log.info "${id} resources will be loaded from ${root}"
			builder.call new FilesystemStorage(root: root)
		} else {
			throw new IllegalArgumentException("Could not open resource directory ${root}")
		}
	}
}
