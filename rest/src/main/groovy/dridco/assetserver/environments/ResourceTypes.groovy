package dridco.assetserver.environments

import dridco.assetserver.ResourceType

public interface ResourceTypes {

	ResourceType javascript()

	ResourceType stylesheets()

	ResourceType images()
}
