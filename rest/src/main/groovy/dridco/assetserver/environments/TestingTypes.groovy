package dridco.assetserver.environments

import static dridco.assetserver.Profiles.TESTING

import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile

import dridco.assetserver.NoOpFilter

@Configuration
@Profile(TESTING)
class TestingTypes extends VolatileEnvironmentTypes {

	@Override
	def javascriptFilter() {
		new NoOpFilter()
	}

	@Override
	def stylesheetFilter() {
		new NoOpFilter()
	}
}
