package dridco.assetserver

import static org.springframework.http.HttpMethod.*
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS

import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter

@Configuration
@EnableWebSecurity
class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

	public static final String DEFAULT_USERNAME = 'asset-deployer'
	public static final String DEFAULT_PASSWORD = 'unsafe.should-change-me'
	public static final String CAN_DELETE_ASSET = 'DELETE'
	public static final String CAN_UPLOAD_ASSET = 'UPLOAD'
	       static final String ANY_URL          = "/**"

	@Override
	protected void configure(AuthenticationManagerBuilder auth) {
		def user     = fromSystemProperty 'asset-server.username', DEFAULT_USERNAME
		def password = fromSystemProperty 'asset-server.password', DEFAULT_PASSWORD

		auth.inMemoryAuthentication()
			.withUser(       user       )
			.password(     password     )
			.roles   ( CAN_DELETE_ASSET )
			.roles   ( CAN_UPLOAD_ASSET )
	}

	static String fromSystemProperty(String propertyName, String defaultValue) {
		System.properties[propertyName] ?: defaultValue
	}

	@Override
	void configure(WebSecurity web) {
		web.ignoring()
			.antMatchers( GET,  ANY_URL )
			.antMatchers( HEAD, ANY_URL )
	}

	@Override
	protected void configure(HttpSecurity http) {
		http
			.csrf().disable()
			.httpBasic().realmName('Asset Server').and()
			.sessionManagement().sessionCreationPolicy(STATELESS)
		.and().authorizeRequests()
			.antMatchers( POST,   ANY_URL ).authenticated()
			.antMatchers( PUT,    ANY_URL ).authenticated()
			.antMatchers( DELETE, ANY_URL ).authenticated()
	}
}
