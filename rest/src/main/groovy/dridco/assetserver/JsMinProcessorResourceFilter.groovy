package dridco.assetserver

import groovy.transform.ToString
import ro.isdc.wro.model.resource.processor.impl.js.JSMinProcessor

@ToString
class JsMinProcessorResourceFilter extends Wro4jProcessorResourceFilter {

	@Override
	JSMinProcessor processor() {
		new JSMinProcessor()
	}
}
