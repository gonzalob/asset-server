package dridco.assetserver

import groovy.util.logging.Slf4j

@Slf4j
class AssetServer {

	def types = [:]

	def of(type) {
		types.withDefault { throw new UnsupportedResourceType(type: type) }[type]
	}
}
