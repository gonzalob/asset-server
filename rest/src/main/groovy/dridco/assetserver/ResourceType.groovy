package dridco.assetserver

import groovy.util.logging.Slf4j

@Slf4j
abstract class ResourceType {

	ResourceFilter filter  = new NoOpFilter()
	       Storage storage = new HashMapStorage()

	abstract process(resources)

	def list() {
		storage.keys()
	}

	void store(Serializable identifier, Serializable source) {
		storage.store identifier, source
	}

	def load(Iterable<ContentSource> sources, boolean skipFilters = false) {
		try {
			def loaded = sources.collect { it.getContent(storage) }
			def source = process loaded
			skipFilters ? source : filter.filter(source)
		} catch(ResourceNotFound e) {
			log.debug "Requested resource ${e.id} does not exist"
			throw e
		}
	}

	def remove(Serializable identifier) {
		storage.delete identifier
	}
}
