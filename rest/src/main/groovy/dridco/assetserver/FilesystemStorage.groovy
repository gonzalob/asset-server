package dridco.assetserver

import static java.util.Arrays.asList
import static java.util.Collections.unmodifiableList

class FilesystemStorage implements Storage {

	File root

	@Override
	Collection<Serializable> keys() {
		unmodifiableList asList(root.list())
	}

	@Override
	Serializable load(Serializable key) throws ResourceNotFound {
		def resource = resource key
		if (resource.exists())
			resource.bytes
		else
			throw new ResourceNotFound()
	}

	private File resource(Serializable key) {
		new File(root, key as String)
	}

	@Override
	void store(Serializable key, Serializable value) {
		def target = resource(key)
		target.delete()
		target << value
	}

	@Override
	void delete(Serializable key) throws ResourceNotFound {
		resource(key).delete()
	}
}
