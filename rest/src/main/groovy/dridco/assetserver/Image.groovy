package dridco.assetserver

class Image extends ResourceType {

	@Override
	def process(resources) {
		assert resources.size() == 1, 'Images can only be retrieved one at a time'
		resources.get(0)
	}
}
