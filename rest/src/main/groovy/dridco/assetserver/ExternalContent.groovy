package dridco.assetserver

class ExternalContent implements ContentSource {

	def content

	@Override
	def getContent(Storage unused) {
		content
	}
}
