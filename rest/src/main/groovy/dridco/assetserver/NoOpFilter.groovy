package dridco.assetserver

import groovy.transform.ToString

@ToString
class NoOpFilter implements ResourceFilter {

	@Override
	def filter(Object source) { source }
}
