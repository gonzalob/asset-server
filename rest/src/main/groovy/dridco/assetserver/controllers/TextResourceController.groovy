package dridco.assetserver.controllers

import static com.google.common.net.HttpHeaders.RANGE
import static dridco.assetserver.controllers.ControllerConstants.*
import static dridco.assetserver.model.Constants.*
import static org.apache.commons.lang3.StringUtils.EMPTY
import static org.springframework.http.HttpStatus.*
import static org.springframework.web.bind.annotation.RequestMethod.*

import javax.annotation.PostConstruct
import javax.servlet.http.HttpServletResponse

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

import com.google.common.base.Optional

import dridco.assetserver.AssetServer
import dridco.assetserver.model.Commands
import dridco.assetserver.model.ETags
import dridco.assetserver.model.PageRangeParseException
import dridco.assetserver.model.Range
import dridco.assetserver.model.RangeRequired

abstract class TextResourceController implements CommitAware {

	@Autowired
	private AssetServer server
	Commands commands

	abstract String    resourceType()
	abstract MediaType mediaType()

	@PostConstruct
	void commands() {
		def resourceType = server.of(resourceType())
		commands = new Commands(
				controller : this,
				eTagsCache : new ETags(type: resourceType),
				mediaType  : mediaType(),
				resources  : resourceType)

		commit()
	}

	@RequestMapping(method = GET)
	ResponseEntity list(@RequestHeader(value = RANGE, required = false) String page) {
		commands.list(Range.parse(Optional.fromNullable(page)))
	}

	@RequestMapping(params = RESOURCE_PARAMETER_NAME)
	ResponseEntity latest(
			@RequestParam(RESOURCE_PARAMETER_NAME) List<String> latest) {
		commands.redirect latest
	}

	@RequestMapping(method = GET, params = VERSION_PARAMETER_NAME)
	ResponseEntity getVersioned(
			@RequestParam(VERSION_PARAMETER_NAME) List<String> versioned,
			@RequestHeader(value = IF_NONE_MATCH, defaultValue = EMPTY) String eTags,
			@CookieValue(value = SKIP_FILTERS_COOKIE_NAME, defaultValue = SKIP_FILTERS_DEFAULT) boolean skipFilters) {
		commands.get versioned, eTags, skipFilters
	}

	@RequestMapping(method = GET, value = ID_PATH_VARIABLE)
	ResponseEntity getOne(
			@PathVariable String id,
			@RequestHeader(value = IF_NONE_MATCH, defaultValue = EMPTY) String eTags,
			@CookieValue(value = SKIP_FILTERS_COOKIE_NAME, defaultValue = SKIP_FILTERS_DEFAULT) boolean skipFilters) {
		commands.current id, eTags, skipFilters
	}

	@RequestMapping(method = PUT, value = ID_PATH_VARIABLE)
	ResponseEntity store(
			@PathVariable String id,
			@RequestHeader(value = IF_MATCH, defaultValue = EMPTY) String eTags,
			@RequestBody String source) {
		commands.put id, eTags, source
	}

	@RequestMapping(method = HEAD, value = ID_PATH_VARIABLE)
	ResponseEntity headers(
			@PathVariable String id) {
		commands.head id
	}

	@RequestMapping(method = HEAD, params = VERSION_PARAMETER_NAME)
	ResponseEntity headVersioned(
			@RequestParam(VERSION_PARAMETER_NAME) List<String> versioned) {
		commands.head versioned
	}

	@RequestMapping(method = DELETE, value = ID_PATH_VARIABLE)
	ResponseEntity delete(
			@PathVariable String id,
			@RequestHeader(value = IF_MATCH, defaultValue = EMPTY) String eTags) {
		commands.delete id, eTags
	}

	@ExceptionHandler(PageRangeParseException)
	@ResponseStatus(BAD_REQUEST)
	void pageRangeParseException() {}

	@ExceptionHandler(RangeRequired)
	@ResponseStatus(REQUEST_ENTITY_TOO_LARGE)
	void rangeRequired(HttpServletResponse response) {
		response.addHeader 'Accept-Ranges', 'page'
	}

	void commit() {
		commands.resumeNormalOperation()
	}
}
