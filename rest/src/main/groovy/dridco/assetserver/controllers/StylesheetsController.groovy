package dridco.assetserver.controllers

import org.springframework.http.MediaType
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping('/css')
class StylesheetsController extends TextResourceController {

	static final String TYPE = 'css'

	@Override
	String resourceType() {
		'stylesheets'
	}

	@Override
	MediaType mediaType() {
		MediaType.parseMediaType 'text/css'
	}
}
