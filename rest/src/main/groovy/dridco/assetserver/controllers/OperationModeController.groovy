package dridco.assetserver.controllers

import static org.springframework.http.HttpStatus.OK
import static org.springframework.web.bind.annotation.RequestMethod.POST
import groovy.util.logging.Slf4j

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@Slf4j
class OperationModeController {

	@Autowired Collection<CommitAware> participants

	@RequestMapping(method = POST, value = '/commit')
	ResponseEntity commit() {
		participants.each { it.commit() }
		log.info "Modifications committed. Resuming normal operation"
		new ResponseEntity(OK)
	}
}
