package dridco.assetserver.controllers

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo
import static org.springframework.http.HttpStatus.NO_CONTENT
import static org.springframework.web.bind.annotation.RequestMethod.GET

import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping('/')
class RootController {

	@RequestMapping(method = GET)
	HttpEntity links() {
		def headers = new HttpHeaders()

		headers.add 'Link', "<${linkTo(StylesheetsController).toString()}>;title=\"${StylesheetsController.TYPE}\""
		headers.add 'Link', "<${linkTo( JavascriptController).toString()}>;title=\"${ JavascriptController.TYPE}\""
		headers.add 'Link', "<${linkTo(      ImageController).toString()}>;title=\"${      ImageController.TYPE}\""
		new ResponseEntity(headers, NO_CONTENT)
	}
}
