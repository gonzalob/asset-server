package dridco.assetserver.controllers

import static com.google.common.net.HttpHeaders.RANGE
import static org.springframework.web.bind.annotation.RequestMethod.*
import groovy.util.logging.Slf4j

import javax.annotation.PostConstruct

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping

import com.google.common.base.Optional

import dridco.assetserver.AssetServer
import dridco.assetserver.model.Commands
import dridco.assetserver.model.ETags
import dridco.assetserver.model.Range

@Slf4j
@Controller
@RequestMapping('/images')
class ImageController implements CommitAware {

	static final String TYPE = 'images'
	private static String resourceType() { 'images' }

	@Autowired
	private AssetServer server
	private Commands    commands

	@PostConstruct
	void commands() {
		def resourceType = server.of(resourceType())
		commands = new Commands(
				controller : this,
				eTagsCache : new ETags(type: resourceType),
				resources  : resourceType)

		commit()
	}

	@RequestMapping(method = GET)
	ResponseEntity list(@RequestHeader(value = RANGE, required = false) String page) {
		commands.list(Range.parse(Optional.fromNullable(page)))
	}

	@RequestMapping(value = "{id}", method = GET)
	ResponseEntity get(
			@PathVariable String id,
			@RequestHeader(value = 'if-none-match', defaultValue = '') String eTags) {
		commands.current id, eTags, false
	}

	@RequestMapping(value = "{id}", method = PUT)
	ResponseEntity store(
			@PathVariable String id,
			@RequestHeader(value = 'if-match', defaultValue = '') String eTags,
			@RequestBody byte[] source) {
		commands.put id, eTags, source
	}

	@RequestMapping(value = "{id}", method = HEAD)
	ResponseEntity headers(
			@PathVariable String id) {
		commands.head id
	}

	@RequestMapping(value = "{id}", method = DELETE)
	ResponseEntity delete(
			@PathVariable String id,
			@RequestHeader(value = 'if-match', defaultValue = '') String eTags) {
		commands.delete id, eTags
	}

	void commit() {
		commands.resumeNormalOperation()
	}
}
