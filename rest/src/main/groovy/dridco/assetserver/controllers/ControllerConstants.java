package dridco.assetserver.controllers;

public interface ControllerConstants {

	String ID_PATH_VARIABLE     = "{id}";
	String SKIP_FILTERS_DEFAULT = "false";
}
