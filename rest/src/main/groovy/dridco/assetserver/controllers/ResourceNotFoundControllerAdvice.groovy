package dridco.assetserver.controllers

import static org.springframework.http.HttpStatus.NOT_FOUND
import groovy.util.logging.Slf4j

import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus

import dridco.assetserver.ResourceNotFound

@Slf4j
@ControllerAdvice
class ResourceNotFoundControllerAdvice {

	@ExceptionHandler(ResourceNotFound)
	@ResponseStatus(NOT_FOUND)
	static void resourceNotFound(ResourceNotFound e) {
		log.debug "Attempted to retrieve inexisting resource ${e.id}"
	}
}
