package dridco.assetserver.controllers

import org.springframework.http.MediaType
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping('/js')
class JavascriptController extends TextResourceController {

	static final String TYPE = 'js'

	@Override
	String resourceType() {
		'javascript'
	}

	@Override
	MediaType mediaType() {
		MediaType.parseMediaType 'application/javascript'
	}
}
