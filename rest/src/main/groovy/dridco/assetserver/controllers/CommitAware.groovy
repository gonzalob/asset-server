package dridco.assetserver.controllers;

interface CommitAware {

	void commit()
}
