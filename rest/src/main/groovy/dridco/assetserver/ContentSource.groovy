package dridco.assetserver

interface ContentSource {

	def getContent(Storage storage)
}
