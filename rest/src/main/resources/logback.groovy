def appenderName  = 'APPENDER'
def rootLevel

if(System.properties['spring.profiles.active'] == 'production') {
	rootLevel = INFO
	appender(appenderName, FileAppender) {
		file = System.properties['log'] ?: System.properties['user.home'] + '/asset-server.log'
		encoder(PatternLayoutEncoder) { pattern = '%date{ISO8601} [%-5level] %logger{0}: %msg%n' }
	}
} else {
	rootLevel = DEBUG
	appender(appenderName, ConsoleAppender) {
		encoder(PatternLayoutEncoder) { pattern = '%8relative [%-5level] %logger{0}: %msg%n' }
	}
}

root rootLevel, [appenderName]
