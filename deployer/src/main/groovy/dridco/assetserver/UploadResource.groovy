package dridco.assetserver

import static groovyx.net.http.ContentType.BINARY
import static groovyx.net.http.Method.*
import static java.util.Arrays.asList
import static org.apache.http.HttpHeaders.AUTHORIZATION
import groovy.util.logging.Slf4j
import groovyjarjarcommonscli.Option
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.URIBuilder

import org.apache.http.HttpRequest
import org.apache.http.HttpRequestInterceptor
import org.apache.http.protocol.HttpContext

@Slf4j
class UploadResource {

    HTTPBuilder http
    def source

    def refresh() {
        def currentResources = queryServer()
        log.debug "Asset Server is holding ${currentResources.keySet()}"
        def newResources     = asList(new File(source).list())
        log.debug "Deployer knows ${newResources}"

        remove           currentResources.findAll { !    newResources.contains   (it.key) }
        refreshIfUpdated newResources    .findAll {  currentResources.containsKey(it    ) }, currentResources
        addNewResources  newResources    .findAll { !currentResources.containsKey(it    ) }
    }

    void addNewResources(created) {
        created.each { add it, content(it) }
    }

    byte[] content(id) {
        new File("${source}/${id}").bytes
    }

    void refreshIfUpdated(toCheck, current) {
        toCheck.each { toUpload ->
            def newContent = content toUpload
            http.request(GET, BINARY) {
                headers.'Cookie' = 'SKIP_ASSET_SERVER_FILTERS=true'
                             uri = current.get toUpload
                response.success = { r, content ->
                          def eTag = r.headers['etag'].value
                    def oldContent = content.bytes
                    if(oldContent == newContent) { log.debug "Skipping up-to-date resource ${toUpload}" }
                    else {
                        log.debug "Uploading updated resource ${toUpload} to replace existing one, with eTag ${eTag}"
                        http.request(PUT, BINARY) {
                            headers.'if-match' = eTag
                                           uri = current.get toUpload
                                          body = newContent
                              response.success = { log.info "Updated resource ${toUpload}" }
                              response.failure = processingFailed
                        }
                    }
                }
                response.failure = processingFailed
            }
        }
    }

    void add(name, source) {
        log.debug "Asset ${name} does not exist. Uploading"
        http.request(PUT, BINARY) {
                    uri.path = name
                        body = source
            response.success = { log.info "New asset ${name} successfully created" }
            response.failure = processingFailed
        }
    }

    void remove(obsolete) {
        obsolete.each { name, url ->
            http.request(HEAD) {
                        uri.path = url
                response.success = {
                    def eTag = it.headers['etag'].value
                    log.debug "Attempting removal of obsolete resource ${name} with eTag ${eTag}"
                    http.request(DELETE) {
                        headers.'if-match' = eTag
                                  uri.path = url
                          response.success = { log.info "Removed obsolete resource ${name}" }
                          response.failure = processingFailed
                    }
                }
                response.failure = processingFailed
            }
        }
    }

    def processingFailed = { log.error "Processing of resource failed. Response status. Response status: '${it.statusLine}'" }

    static void main(String[] args) {
        def builder = new CliBuilder(usage: UploadResource.name)
        def cli = builder.with {
            b longOpt: 'base'   , args: Option.UNLIMITED_VALUES, required: true, 'Base URI for the Asset Server. May be used more than once'
            s longOpt: 'sources', args: Option.UNLIMITED_VALUES, required: true, 'Type and directory with sources to upload, splitted by ":". May be used more than once'
            u longOpt: 'user'   , args:                       1, required: true, 'Username to authenticate to the Asset Server'
            p longOpt: 'pass'   , args:                       1, required: true, 'Password to authenticate to the Asset Server'
            h longOpt: 'help'   ,                                                'This help screen'

            parse args
        }

        if (!cli  ) {                  return }
        if ( cli.h) { builder.usage(); return }

		cli.bs.each { base ->
	        log.info "Asset Server: ${base}"
			def httpBuilder = httpBuilder(base, cli.u, cli.p)
			def types       = types httpBuilder
			def serverBase  = serverBase httpBuilder
			
			cli.ss.each { source ->
				def sources = source.split ':'
				def type = sources[0]
				def path = sources[1]
		        if (!types.contains(type)) { builder.usage(); return }
		        def resourceBase = "${serverBase[type]}/"
		        log.info "Resources Base: ${resourceBase}"
				httpBuilder.uri = resourceBase
				
				new UploadResource(http: httpBuilder, source: path).refresh()
	        }
		}

		cli.bs.each { serverUri ->
			URIBuilder commitUri = new URIBuilder(serverUri)
			commitUri.path += '/commit'
			httpBuilder(serverUri, cli.u, cli.p).post(path: commitUri) {
				log.info "Committed changes to ${serverUri}"
			}
		}
    }

	private static httpBuilder(base, user, pass) {
		def builder = new HTTPBuilder(base)
        builder.client.addRequestInterceptor preemptiveAuthorization(user, pass)
		builder
	}

    private static HttpRequestInterceptor preemptiveAuthorization(username, password) {
        new HttpRequestInterceptor() {
            @Override void process(HttpRequest request, HttpContext context) {
                request.addHeader(AUTHORIZATION, 'Basic ' + "${username}:${password}".bytes.encodeBase64().toString())
            }
        }
    }

    private static Collection<String> types(HTTPBuilder httpBuilder) {
        httpBuilder.request(GET) {
            response.success = { it.getHeaders('link').collect { it.elements*.getParameterByName('title').value }.flatten() }
            response.failure = cannotConnect
        }
    }

    private static serverBase(HTTPBuilder httpBuilder) {
        httpBuilder.request(GET) {
            response.success = resourcesAndLocations
            response.failure = cannotConnect
        }
    }

    static cannotConnect         = { throw new IllegalArgumentException('Cannot connect to Asset Server') }
    static resourcesAndLocations = { it.getHeaders('link').collectEntries {
        def eachType = it.elements*.getParameterByName('title').value.get(0)
        def eachUrl  = it.elements[0].name
        [ eachType, removeSurroundingMarkers(eachUrl) ]
    } }

    private static String removeSurroundingMarkers(value) {
        value.substring 1, value.length() - 1
    }

    private def queryServer() {
		collectPagedResources(1)
    }
	
	private def collectPagedResources(page) {
		log.debug "Collecting resources from page ${page}"
        def resourcesInPage = http.request(GET) {
			 headers.'Range' = "page=${page}"
			response.success = {
				def resourcesInThisPage = resourcesAndLocations it
				def contentRange = it.getHeaders('content-range')[0]
				if(contentRange && page < pageNumberFrom(contentRange)) {
					resourcesInThisPage += collectPagedResources(page + 1)
				}
				resourcesInThisPage
			 }
		}
	}
	
	private def pageNumberFrom(contentRange) {
		def range = contentRange.value
		Integer.valueOf range.substring(range.indexOf('/') + 1)
	}
}
